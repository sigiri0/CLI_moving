import numpy as np 
import os


class maps():
	def maps():
		masp = np.array([
			[2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
			[2, 0, 2, 0, 2, 0, 0, 0, 0, 2],
			[2, 0, 2, 0, 2, 0, 2, 0, 0, 2],
			[2, 0, 2, 0, 2, 2, 2, 2, 0, 2],
			[2, 0, 2, 0, 0, 0, 0, 0, 0, 2],
			[2, 0, 2, 0, 2, 2, 2, 2, 0, 2],
			[2, 0, 2, 0, 2, 0, 0, 2, 0, 2],
			[2, 0, 2, 0, 2, 0, 2, 2, 0, 2],
			[2, 0, 0, 0, 2, 0, 0, 0, 0, 2],
			[2, 2, 2, 2, 2, 2, 2, 2, 2, 2]])

		return masp

class main:
	def moving(**kwargs):
		try:
			x = kwargs['x']
			y = kwargs['y']

			
			move =  input(kwargs['message'])

			if move == kwargs['keys'][0]:
				matrix = kwargs['maps']

				if matrix[y-1][x] != kwargs['wall']:
					y -= 1

			elif move == kwargs['keys'][1]:
				matrix = kwargs['maps']

				if matrix[y][x-1] != kwargs['wall']:
					x -= 1

			elif move == kwargs['keys'][2]:
				matrix = kwargs['maps']

				if matrix[y+1][x] != kwargs['wall']:
					y += 1

			elif move == kwargs['keys'][3]:
				matrix = kwargs['maps']

				if matrix[y][x+1] != kwargs['wall']:
					x += 1

			else:
				print(f'({" ".join(kwargs["keys"])})')

			if move:
				matrix[y][x] = kwargs['player']

				return matrix, x, y

		except:
			kwargs['maps'][kwargs['y']][kwargs['x']] = kwargs['player']

			return kwargs['maps'], kwargs['x'], kwargs['y']


if __name__ == '__main__':
	x = 1
	y = 1

	while True:
		xoi = main.moving(x = x,
			y = y,
			message='::: ',
			player=1,
			wall=2,
			keys=['w', 'a', 's', 'd'],
			maps=maps.maps())

		#for linux
		os.system('clear')

		print(xoi[0])

		x = xoi[1]

		y = xoi[2]